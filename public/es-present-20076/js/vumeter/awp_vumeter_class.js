/* awp_vumeter_class.js: AudioWorkletGlobalScope */
const MINIMUM_VALUE = 0.00001;
const NSAMPLES_PER_FRAME = 1;
// See: https://stackoverflow.com/a/19589884/3621841
// For PCM the sample rate and the frame rate are the same since a frame consists of 1 sample from each channel:
//   Sample rate = number of samples / second
//   Frame = 1 sample from each channel (PCM)
//   Frame Size = Sample size * Channels
//   Frame Rate = frames / second

registerProcessor(
  'awp_vumeter_class',
  class extends AudioWorkletProcessor {
    constructor(options) {
      super();
      options.processorOptions.update_interval_ms = parseInt(
        options.processorOptions.update_interval_ms
      );
      if (
        isNaN(options.processorOptions.update_interval_ms) == true ||
        options.processorOptions.update_interval_ms <= 0
      )
        options.processorOptions.update_interval_ms = 1000;
      options.processorOptions.rms_peak_stay_ms = parseInt(
        options.processorOptions.rms_peak_stay_ms
      );
      if (
        isNaN(options.processorOptions.rms_peak_stay_ms) == true ||
        options.processorOptions.rms_peak_stay_ms <= 0
      )
        options.processorOptions.rms_peak_stay_ms = 1000 * 3;
      console.log('[awp_vumeter_class] - ' + JSON.stringify(options));
      this._rms;
      this._rms_peak;
      this._rms_peak_last_updated_timestamp;
      this._audio_worklet_node_id = options.processorOptions.audio_worklet_node_id;
      this._update_interval_ms = options.processorOptions.update_interval_ms;
      this._rms_peak_stay_ms = options.processorOptions.rms_peak_stay_ms;
      this._rms_peak_stay_ms = Math.floor(this._rms_peak_stay_ms / this._update_interval_ms);
      if (this._rms_peak_stay_ms < 1) this._rms_peak_stay_ms = 1;
      this._rms_peak_stay_ms = this._rms_peak_stay_ms * this._update_interval_ms;
      this._next_update_frame = 0 + this.convert_update_interval_ms_to_nframes; // We will update when we process the frame number 0 + nframes (in _update_interval_ms duration)
      // Receive message from the associated AudioWorkletNode (the 'vumeter_node' in our case)
      this.port.onmessage = event => {
        if (event.data.audio_worklet_node_id != this._audio_worklet_node_id) return;
        if (event.data.update_interval_ms) this._update_interval_ms = event.data.update_interval_ms;
      };
    }
    get convert_update_interval_ms_to_nframes() {
      // See: https://stackoverflow.com/a/19589884/3621841
      var nsamples = (this._update_interval_ms / 1000) * sampleRate; // sampleRate is one of the variables in AudioWorkletGlobalScope (which is the scope shared across AudioWorkletProcessors. Other variables include: currentFrame / currentTime / sampleRate (see: https://developer.mozilla.org/en-US/docs/Web/API/AudioWorkletGlobalScope)
      var nframes = nsamples / NSAMPLES_PER_FRAME;
      return nframes;
    }
    process(inputs, outputs, parameters) {
      // inputs[iinput][ichannel][isample]
      // This method gets called for each block of 128 sample-frames and takes input and output arrays and calculated values of custom AudioParams (if they are defined) as parameters.
      // You can use inputs and audio parameter values to fill the outputs array, which by default holds silence.
      // Each sample value is in range of [-1, +1]  (with decimals, since we get a Float32Array)
      // So values are normalized. BUT!!!! You can actually get values out of the [-1, +1] range.
      // For instance if you have 5 oscillators connected to 1 input of an audio node, all those 5 inputs will be mixed down so levels will add-up, giving you values in the [-5, +5] range.
      // But any values out of the [-1, +1] range are considered as clipped

      if (false) {
        console.log(
          '[process] - ' +
            'currentTime:[' +
            currentTime +
            ']' +
            '  currentFrame:[' +
            currentFrame +
            ']' +
            '  sampleRate:[' +
            sampleRate +
            ']' +
            '  ninputs:[' +
            inputs.length +
            ']' +
            '  input0.nchannels:[' +
            (inputs[0] != null && inputs[0].length >= 0 ? inputs[0].length : 0) +
            ']' +
            '  input0.channel0.nsamples:[' +
            (inputs[0] != null &&
            inputs[0].length >= 0 &&
            inputs[0][0] != null &&
            inputs[0][0].length >= 0
              ? inputs[0][0].length
              : 0) +
            ']' +
            '  input0.channel0.sample0:[' +
            (inputs[0] != null && inputs[0][0] != null && inputs[0][0][0] != null
              ? inputs[0][0][0]
              : 0) +
            ']' +
            ''
        );
      }

      // See: vumeter_node_class.js where we specify numberOfInputs==I, channelCount==C, which means that all connections will be mixed to just I inputs, each one of them with C channels (so channels from each connections are also mixed to C channels)
      // NOTE: If 0 inputs are connected then 0 channels will be passed in.
      var ninputs,
        nchannels,
        _nchannels,
        inputI_channels,
        inputI_channelC_samples,
        inputI_channelC_nsamples,
        i,
        c,
        s,
        sum,
        rms,
        current_timestamp;

      // Initialize this._rms and this._rms_peak if they have NOT been initialized yet
      try {
        ninputs = inputs.length;
      } catch (e) {
        ninputs = 0;
      }
      for (i = 0; i < ninputs; i++) {
        inputI_channels = inputs[i];
        try {
          _nchannels = inputI_channels.length;
        } catch (e) {
          _nchannels = 0;
        }
        if (nchannels == null || nchannels < _nchannels) nchannels = _nchannels;
      }
      if (
        this._rms == null ||
        this._rms_peak == null ||
        this._rms_peak_last_updated_timestamp == null ||
        this._rms.length != ninputs ||
        (this._rms[0] != null && this._rms[0].length != nchannels)
      ) {
        this._rms = [];
        this._rms_peak = [];
        this._rms_peak_last_updated_timestamp = [];
        for (i = 0; i < ninputs; i++) {
          this._rms[i] = [];
          this._rms_peak[i] = [];
          this._rms_peak_last_updated_timestamp[i] = [];
          for (c = 0; c < nchannels; c++) {
            this._rms[i][c] = 0;
            this._rms_peak[i][c] = 0;
            this._rms_peak_last_updated_timestamp[i][c] = null;
          }
        }
        //console.log(this._rms, this._rms_peak);
      }

      // Start processing inputs
      current_timestamp = Date.now(); // performance is not available in AudioWorkletProcessors
      try {
        ninputs = inputs.length;
      } catch (e) {
        ninputs = 0;
      }
      for (i = 0; i < ninputs; i++) {
        inputI_channels = inputs[i];
        try {
          nchannels = inputI_channels.length;
        } catch (e) {
          nchannels = 0;
        }
        for (c = 0; c < nchannels; c++) {
          inputI_channelC_samples = inputI_channels[c];
          if (
            inputI_channelC_nsamples == null ||
            inputI_channelC_nsamples < inputI_channelC_samples.length
          )
            inputI_channelC_nsamples = inputI_channelC_samples.length;

          // Calculate the squared-sum.
          sum = 0;
          rms = 0;
          for (s = 0; s < inputI_channelC_samples.length; ++s)
            sum += inputI_channelC_samples[s] * inputI_channelC_samples[s]; // Note this will also get rid of negative values, so the new range will be [0, 1*1]
          // Calculate the RMS level and update the volume.
          var ref = 1 / Math.sqrt(2);
          // A full-sin wave    should have an RMS equal to 0 dBFs (See: https://en.wikipedia.org/wiki/DBFS)
          // A full-square wave should have an RMS equal to 3 dBFs (See: https://en.wikipedia.org/wiki/DBFS)
          // The ref is the RMS value of a full-sin wave, which is Math.sqrt((0+1+0+1+0)/4) = Math.sqrt(1/2) = 0.7071067811865476   See: https://www.electronics-tutorials.ws/accircuits/rms-voltage.html
          // This is done so that 20 * Math.log10( rms_of_full_sin / ref ) == 0
          // Since rms_of_full_sin == 0.7071, then  0 dBFs = 20 * Math.log10( rms_of_full_sin / ref ) = 20 * Math.log10( 0.7071 / ref )  ==>  ref = 0.7071
          // So if whe receive an RMS value, we need to divide it by this reference
          rms =
            inputI_channelC_samples.length > 0
              ? Math.sqrt(sum / inputI_channelC_samples.length) / ref
              : 0;
          if (
            this._rms_peak[i][c] == null ||
            this._rms_peak[i][c] < rms ||
            (this._rms_peak[i][c] >= rms &&
              (this._rms_peak_last_updated_timestamp[i][c] == null ||
                current_timestamp - this._rms_peak_last_updated_timestamp[i][c] >=
                  this._rms_peak_stay_ms))
          ) {
            this._rms_peak[i][c] = rms;
            this._rms_peak_last_updated_timestamp[i][c] = Date.now();
          }
          this._rms[i][c] = rms; // Fast attack
        }
      }

      // Update and sync the volume property with the main thread.
      if (inputI_channelC_nsamples != null) {
        this._next_update_frame -= inputI_channelC_nsamples / NSAMPLES_PER_FRAME;
        if (this._next_update_frame < 0) {
          this._next_update_frame += this.convert_update_interval_ms_to_nframes; // We will update when we process the frame number current_frame + nframes (in _update_interval_ms duration)
          //console.log('  [process] - rms:'+JSON.stringify(this._rms)+'  rms_peak:'+JSON.stringify(this._rms_peak)+'');
          // Send message to the associated AudioWorkletNode (the 'vumeter_node' in our case)
          this.port.postMessage({
            audio_worklet_node_id: this._audio_worklet_node_id,
            rms: this._rms,
            rms_peak: this._rms_peak,
          });
        }
      }

      // Keep on processing if the volume is above a threshold, so that disconnecting inputs does not immediately cause the meter to stop computing its smoothed value
      var keep_processing = false;
      /*for (i=0; keep_processing==false && i<ninputs; i++) {
      for (c=0; keep_processing==false && c<nchannels; c++) {
        if (this._rms[i][c]>=MINIMUM_VALUE)  { keep_processing = true; break; }
      }
    } */
      keep_processing = true;

      return keep_processing;
    }
  }
);
