//import { OpusStreamDecoder } from './opus-stream-decoder.es6.js';  // See: https://web.dev/module-workers/
////import { decoded_audio_playback_buffer_class } from 'decoded-audio-playback-buffer.mjs';

// IMPORTANT!!!!!
// See JIRA http://jira.on24.com/browse/PRESENT-15422
// There is a bug in Firefox preventing playback to work. See:
// 
// https://bugzilla.mozilla.org/show_bug.cgi?id=1247687
// https://bugzilla.mozilla.org/show_bug.cgi?id=1558780
// https://stackoverflow.com/questions/52504434/does-firefox-really-support-the-type-option-for-the-worker-constructor|https://bugzilla.mozilla.org/show_bug.cgi?id=1247687]
//
// Basically FF does not allow imports in worker modules.
// So in our particular case, we cannot import 'opus-stream-decoder.es6.js' into this worker file.
// The TEMPORARY solution is to put all the code from 'opus-stream-decoder.es6.js' in this file.
// This way we are not importing it and FF will not throw an error.

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// [opus-stream-decoder.es6.js] - start

var Module = typeof Module !== "undefined" ? Module : {};
Module["locateFile"] = function(filename) {
    return ENVIRONMENT_IS_NODE ? __dirname + "/" + filename : filename
}
;
var moduleOverrides = {};
var key;
for (key in Module) {
    if (Module.hasOwnProperty(key)) {
        moduleOverrides[key] = Module[key]
    }
}
var arguments_ = [];
var thisProgram = "./this.program";
var quit_ = function(status, toThrow) {
    throw toThrow
};
var ENVIRONMENT_IS_WEB = false;
var ENVIRONMENT_IS_WORKER = false;
var ENVIRONMENT_IS_NODE = false;
var ENVIRONMENT_HAS_NODE = false;
var ENVIRONMENT_IS_SHELL = false;
ENVIRONMENT_IS_WEB = typeof window === "object";
ENVIRONMENT_IS_WORKER = typeof importScripts === "function";
ENVIRONMENT_HAS_NODE = typeof process === "object" && typeof process.versions === "object" && typeof process.versions.node === "string";
ENVIRONMENT_IS_NODE = ENVIRONMENT_HAS_NODE && !ENVIRONMENT_IS_WEB && !ENVIRONMENT_IS_WORKER;
ENVIRONMENT_IS_SHELL = !ENVIRONMENT_IS_WEB && !ENVIRONMENT_IS_NODE && !ENVIRONMENT_IS_WORKER;
var scriptDirectory = "";
function locateFile(path) {
    if (Module["locateFile"]) {
        return Module["locateFile"](path, scriptDirectory)
    }
    return scriptDirectory + path
}
var read_, readAsync, readBinary, setWindowTitle;
var nodeFS;
var nodePath;
if (ENVIRONMENT_IS_NODE) {
    scriptDirectory = __dirname + "/";
    read_ = function shell_read(filename, binary) {
        if (!nodeFS)
            nodeFS = require("fs");
        if (!nodePath)
            nodePath = require("path");
        filename = nodePath["normalize"](filename);
        return nodeFS["readFileSync"](filename, binary ? null : "utf8")
    }
    ;
    readBinary = function readBinary(filename) {
        var ret = read_(filename, true);
        if (!ret.buffer) {
            ret = new Uint8Array(ret)
        }
        assert(ret.buffer);
        return ret
    }
    ;
    if (process["argv"].length > 1) {
        thisProgram = process["argv"][1].replace(/\\/g, "/")
    }
    arguments_ = process["argv"].slice(2);
    if (typeof module !== "undefined") {
        module["exports"] = Module
    }
    process["on"]("uncaughtException", function(ex) {
        if (!(ex instanceof ExitStatus)) {
            throw ex
        }
    });
    process["on"]("unhandledRejection", abort);
    quit_ = function(status) {
        process["exit"](status)
    }
    ;
    Module["inspect"] = function() {
        return "[Emscripten Module object]"
    }
} else if (ENVIRONMENT_IS_SHELL) {
    if (typeof read != "undefined") {
        read_ = function shell_read(f) {
            return read(f)
        }
    }
    readBinary = function readBinary(f) {
        var data;
        if (typeof readbuffer === "function") {
            return new Uint8Array(readbuffer(f))
        }
        data = read(f, "binary");
        assert(typeof data === "object");
        return data
    }
    ;
    if (typeof scriptArgs != "undefined") {
        arguments_ = scriptArgs
    } else if (typeof arguments != "undefined") {
        arguments_ = arguments
    }
    if (typeof quit === "function") {
        quit_ = function(status) {
            quit(status)
        }
    }
    if (typeof print !== "undefined") {
        if (typeof console === "undefined")
            console = {};
        console.log = print;
        console.warn = console.error = typeof printErr !== "undefined" ? printErr : print
    }
} else if (ENVIRONMENT_IS_WEB || ENVIRONMENT_IS_WORKER) {
    if (ENVIRONMENT_IS_WORKER) {
        scriptDirectory = self.location.href
    } else if (document.currentScript) {
        scriptDirectory = document.currentScript.src
    }
    if (scriptDirectory.indexOf("blob:") !== 0) {
        scriptDirectory = scriptDirectory.substr(0, scriptDirectory.lastIndexOf("/") + 1)
    } else {
        scriptDirectory = ""
    }
    {
        read_ = function shell_read(url) {
            var xhr = new XMLHttpRequest;
            xhr.open("GET", url, false);
            xhr.send(null);
            return xhr.responseText
        }
        ;
        if (ENVIRONMENT_IS_WORKER) {
            readBinary = function readBinary(url) {
                var xhr = new XMLHttpRequest;
                xhr.open("GET", url, false);
                xhr.responseType = "arraybuffer";
                xhr.send(null);
                return new Uint8Array(xhr.response)
            }
        }
        readAsync = function readAsync(url, onload, onerror) {
            var xhr = new XMLHttpRequest;
            xhr.open("GET", url, true);
            xhr.responseType = "arraybuffer";
            xhr.onload = function xhr_onload() {
                if (xhr.status == 200 || xhr.status == 0 && xhr.response) {
                    onload(xhr.response);
                    return
                }
                onerror()
            }
            ;
            xhr.onerror = onerror;
            xhr.send(null)
        }
    }
    setWindowTitle = function(title) {
        document.title = title
    }
} else {}
var out = Module["print"] || console.log.bind(console);
var err = Module["printErr"] || console.warn.bind(console);
for (key in moduleOverrides) {
    if (moduleOverrides.hasOwnProperty(key)) {
        Module[key] = moduleOverrides[key]
    }
}
moduleOverrides = null;
if (Module["arguments"])
    arguments_ = Module["arguments"];
if (Module["thisProgram"])
    thisProgram = Module["thisProgram"];
if (Module["quit"])
    quit_ = Module["quit"];
var wasmBinary;
if (Module["wasmBinary"])
    wasmBinary = Module["wasmBinary"];
var noExitRuntime;
if (Module["noExitRuntime"])
    noExitRuntime = Module["noExitRuntime"];
if (typeof WebAssembly !== "object") {
    err("no native wasm support detected")
}
var wasmMemory;
var wasmTable = new WebAssembly.Table({
    "initial": 7,
    "maximum": 7 + 0,
    "element": "anyfunc"
});
var ABORT = false;
var EXITSTATUS = 0;
function assert(condition, text) {
    if (!condition) {
        abort("Assertion failed: " + text)
    }
}
function getCFunc(ident) {
    var func = Module["_" + ident];
    assert(func, "Cannot call unknown function " + ident + ", make sure it is exported");
    return func
}
function ccall(ident, returnType, argTypes, args, opts) {
    var toC = {
        "string": function(str) {
            var ret = 0;
            if (str !== null && str !== undefined && str !== 0) {
                var len = (str.length << 2) + 1;
                ret = stackAlloc(len);
                stringToUTF8(str, ret, len)
            }
            return ret
        },
        "array": function(arr) {
            var ret = stackAlloc(arr.length);
            writeArrayToMemory(arr, ret);
            return ret
        }
    };
    function convertReturnValue(ret) {
        if (returnType === "string")
            return UTF8ToString(ret);
        if (returnType === "boolean")
            return Boolean(ret);
        return ret
    }
    var func = getCFunc(ident);
    var cArgs = [];
    var stack = 0;
    if (args) {
        for (var i = 0; i < args.length; i++) {
            var converter = toC[argTypes[i]];
            if (converter) {
                if (stack === 0)
                    stack = stackSave();
                cArgs[i] = converter(args[i])
            } else {
                cArgs[i] = args[i]
            }
        }
    }
    var ret = func.apply(null, cArgs);
    ret = convertReturnValue(ret);
    if (stack !== 0)
        stackRestore(stack);
    return ret
}
function cwrap(ident, returnType, argTypes, opts) {
    argTypes = argTypes || [];
    var numericArgs = argTypes.every(function(type) {
        return type === "number"
    });
    var numericRet = returnType !== "string";
    if (numericRet && numericArgs && !opts) {
        return getCFunc(ident)
    }
    return function() {
        return ccall(ident, returnType, argTypes, arguments, opts)
    }
}
var UTF8Decoder = typeof TextDecoder !== "undefined" ? new TextDecoder("utf8") : undefined;
function UTF8ArrayToString(u8Array, idx, maxBytesToRead) {
    var endIdx = idx + maxBytesToRead;
    var endPtr = idx;
    while (u8Array[endPtr] && !(endPtr >= endIdx))
        ++endPtr;
    if (endPtr - idx > 16 && u8Array.subarray && UTF8Decoder) {
        return UTF8Decoder.decode(u8Array.subarray(idx, endPtr))
    } else {
        var str = "";
        while (idx < endPtr) {
            var u0 = u8Array[idx++];
            if (!(u0 & 128)) {
                str += String.fromCharCode(u0);
                continue
            }
            var u1 = u8Array[idx++] & 63;
            if ((u0 & 224) == 192) {
                str += String.fromCharCode((u0 & 31) << 6 | u1);
                continue
            }
            var u2 = u8Array[idx++] & 63;
            if ((u0 & 240) == 224) {
                u0 = (u0 & 15) << 12 | u1 << 6 | u2
            } else {
                u0 = (u0 & 7) << 18 | u1 << 12 | u2 << 6 | u8Array[idx++] & 63
            }
            if (u0 < 65536) {
                str += String.fromCharCode(u0)
            } else {
                var ch = u0 - 65536;
                str += String.fromCharCode(55296 | ch >> 10, 56320 | ch & 1023)
            }
        }
    }
    return str
}
function UTF8ToString(ptr, maxBytesToRead) {
    return ptr ? UTF8ArrayToString(HEAPU8, ptr, maxBytesToRead) : ""
}
function stringToUTF8Array(str, outU8Array, outIdx, maxBytesToWrite) {
    if (!(maxBytesToWrite > 0))
        return 0;
    var startIdx = outIdx;
    var endIdx = outIdx + maxBytesToWrite - 1;
    for (var i = 0; i < str.length; ++i) {
        var u = str.charCodeAt(i);
        if (u >= 55296 && u <= 57343) {
            var u1 = str.charCodeAt(++i);
            u = 65536 + ((u & 1023) << 10) | u1 & 1023
        }
        if (u <= 127) {
            if (outIdx >= endIdx)
                break;
            outU8Array[outIdx++] = u
        } else if (u <= 2047) {
            if (outIdx + 1 >= endIdx)
                break;
            outU8Array[outIdx++] = 192 | u >> 6;
            outU8Array[outIdx++] = 128 | u & 63
        } else if (u <= 65535) {
            if (outIdx + 2 >= endIdx)
                break;
            outU8Array[outIdx++] = 224 | u >> 12;
            outU8Array[outIdx++] = 128 | u >> 6 & 63;
            outU8Array[outIdx++] = 128 | u & 63
        } else {
            if (outIdx + 3 >= endIdx)
                break;
            outU8Array[outIdx++] = 240 | u >> 18;
            outU8Array[outIdx++] = 128 | u >> 12 & 63;
            outU8Array[outIdx++] = 128 | u >> 6 & 63;
            outU8Array[outIdx++] = 128 | u & 63
        }
    }
    outU8Array[outIdx] = 0;
    return outIdx - startIdx
}
function stringToUTF8(str, outPtr, maxBytesToWrite) {
    return stringToUTF8Array(str, HEAPU8, outPtr, maxBytesToWrite)
}
var UTF16Decoder = typeof TextDecoder !== "undefined" ? new TextDecoder("utf-16le") : undefined;
function writeArrayToMemory(array, buffer) {
    HEAP8.set(array, buffer)
}
var WASM_PAGE_SIZE = 65536;
var buffer, HEAP8, HEAPU8, HEAP16, HEAPU16, HEAP32, HEAPU32, HEAPF32, HEAPF64;
function updateGlobalBufferAndViews(buf) {
    buffer = buf;
    Module["HEAP8"] = HEAP8 = new Int8Array(buf);
    Module["HEAP16"] = HEAP16 = new Int16Array(buf);
    Module["HEAP32"] = HEAP32 = new Int32Array(buf);
    Module["HEAPU8"] = HEAPU8 = new Uint8Array(buf);
    Module["HEAPU16"] = HEAPU16 = new Uint16Array(buf);
    Module["HEAPU32"] = HEAPU32 = new Uint32Array(buf);
    Module["HEAPF32"] = HEAPF32 = new Float32Array(buf);
    Module["HEAPF64"] = HEAPF64 = new Float64Array(buf)
}
var DYNAMIC_BASE = 5284544
  , DYNAMICTOP_PTR = 41504;
var INITIAL_TOTAL_MEMORY = Module["TOTAL_MEMORY"] || 16777216;
if (Module["wasmMemory"]) {
    wasmMemory = Module["wasmMemory"]
} else {
    wasmMemory = new WebAssembly.Memory({
        "initial": INITIAL_TOTAL_MEMORY / WASM_PAGE_SIZE,
        "maximum": INITIAL_TOTAL_MEMORY / WASM_PAGE_SIZE
    })
}
if (wasmMemory) {
    buffer = wasmMemory.buffer
}
INITIAL_TOTAL_MEMORY = buffer.byteLength;
updateGlobalBufferAndViews(buffer);
HEAP32[DYNAMICTOP_PTR >> 2] = DYNAMIC_BASE;
function callRuntimeCallbacks(callbacks) {
    while (callbacks.length > 0) {
        var callback = callbacks.shift();
        if (typeof callback == "function") {
            callback();
            continue
        }
        var func = callback.func;
        if (typeof func === "number") {
            if (callback.arg === undefined) {
                Module["dynCall_v"](func)
            } else {
                Module["dynCall_vi"](func, callback.arg)
            }
        } else {
            func(callback.arg === undefined ? null : callback.arg)
        }
    }
}
var __ATPRERUN__ = [];
var __ATINIT__ = [];
var __ATMAIN__ = [];
var __ATPOSTRUN__ = [];
var runtimeInitialized = false;
function preRun() {
    if (Module["preRun"]) {
        if (typeof Module["preRun"] == "function")
            Module["preRun"] = [Module["preRun"]];
        while (Module["preRun"].length) {
            addOnPreRun(Module["preRun"].shift())
        }
    }
    callRuntimeCallbacks(__ATPRERUN__)
}
function initRuntime() {
    runtimeInitialized = true;
    callRuntimeCallbacks(__ATINIT__)
}
function preMain() {
    callRuntimeCallbacks(__ATMAIN__)
}
function postRun() {
    if (Module["postRun"]) {
        if (typeof Module["postRun"] == "function")
            Module["postRun"] = [Module["postRun"]];
        while (Module["postRun"].length) {
            addOnPostRun(Module["postRun"].shift())
        }
    }
    callRuntimeCallbacks(__ATPOSTRUN__)
}
function addOnPreRun(cb) {
    __ATPRERUN__.unshift(cb)
}
function addOnPreMain(cb) {
    __ATMAIN__.unshift(cb)
}
function addOnPostRun(cb) {
    __ATPOSTRUN__.unshift(cb)
}
var Math_abs = Math.abs;
var runDependencies = 0;
var runDependencyWatcher = null;
var dependenciesFulfilled = null;
function addRunDependency(id) {
    runDependencies++;
    if (Module["monitorRunDependencies"]) {
        Module["monitorRunDependencies"](runDependencies)
    }
}
function removeRunDependency(id) {
    runDependencies--;
    if (Module["monitorRunDependencies"]) {
        Module["monitorRunDependencies"](runDependencies)
    }
    if (runDependencies == 0) {
        if (runDependencyWatcher !== null) {
            clearInterval(runDependencyWatcher);
            runDependencyWatcher = null
        }
        if (dependenciesFulfilled) {
            var callback = dependenciesFulfilled;
            dependenciesFulfilled = null;
            callback()
        }
    }
}
Module["preloadedImages"] = {};
Module["preloadedAudios"] = {};
function abort(what) {
    if (Module["onAbort"]) {
        Module["onAbort"](what)
    }
    what += "";
    out(what);
    err(what);
    ABORT = true;
    EXITSTATUS = 1;
    what = "abort(" + what + "). Build with -s ASSERTIONS=1 for more info.";
    throw new WebAssembly.RuntimeError(what)
}
var dataURIPrefix = "data:application/octet-stream;base64,";
function isDataURI(filename) {
    return String.prototype.startsWith ? filename.startsWith(dataURIPrefix) : filename.indexOf(dataURIPrefix) === 0
}
var wasmBinaryFile = "opus-stream-decoder.wasm";
if (!isDataURI(wasmBinaryFile)) {
    wasmBinaryFile = locateFile(wasmBinaryFile)
}
function getBinary() {
    try {
        if (wasmBinary) {
            return new Uint8Array(wasmBinary)
        }
        if (readBinary) {
            return readBinary(wasmBinaryFile)
        } else {
            throw "both async and sync fetching of the wasm failed"
        }
    } catch (err) {
        abort(err)
    }
}
function getBinaryPromise() {
    if (!wasmBinary && (ENVIRONMENT_IS_WEB || ENVIRONMENT_IS_WORKER) && typeof fetch === "function") {
        return fetch(wasmBinaryFile, {
            credentials: "same-origin",
            headers: { "Content-Type": "application/wasm" }   // [Alex]
        }).then(function(response) {
            if (!response["ok"]) {
                throw "failed to load wasm binary file at '" + wasmBinaryFile + "'"
            }
            return response["arrayBuffer"]()
        }).catch(function() {
            return getBinary()
        })
    }
    return new Promise(function(resolve, reject) {
        resolve(getBinary())
    }
    )
}
function createWasm() {
    var info = {
        "env": asmLibraryArg,
        "wasi_snapshot_preview1": asmLibraryArg
    };
    function receiveInstance(instance, module) {
        var exports = instance.exports;
        Module["asm"] = exports;
        removeRunDependency("wasm-instantiate")
    }
    addRunDependency("wasm-instantiate");
    function receiveInstantiatedSource(output) {
        receiveInstance(output["instance"])
    }
    function instantiateArrayBuffer(receiver) {
        return getBinaryPromise().then(function(binary) {
            return WebAssembly.instantiate(binary, info)
        }).then(receiver, function(reason) {
            err("failed to asynchronously prepare wasm: " + reason);
            abort(reason)
        })
    }
    function instantiateAsync() {
        if (!wasmBinary && typeof WebAssembly.instantiateStreaming === "function" && !isDataURI(wasmBinaryFile) && typeof fetch === "function") {
            fetch(wasmBinaryFile, {
                credentials: "same-origin",
                headers: { "Content-Type": "application/wasm" }   // [Alex]
            }).then(function(response) {
                var result = WebAssembly.instantiateStreaming(response, info);
                return result.then(receiveInstantiatedSource, function(reason) {
                    err("wasm streaming compile failed: " + reason);
                    err("falling back to ArrayBuffer instantiation");
                    instantiateArrayBuffer(receiveInstantiatedSource)
                })
            })
        } else {
            return instantiateArrayBuffer(receiveInstantiatedSource)
        }
    }
    if (Module["instantiateWasm"]) {
        try {
            var exports = Module["instantiateWasm"](info, receiveInstance);
            return exports
        } catch (e) {
            err("Module.instantiateWasm callback failed with error: " + e);
            return false
        }
    }
    instantiateAsync();
    return {}
}
__ATINIT__.push({
    func: function() {
        ___wasm_call_ctors()
    }
});
var _abs = Math_abs;
function _emscripten_memcpy_big(dest, src, num) {
    HEAPU8.set(HEAPU8.subarray(src, src + num), dest)
}
function abortOnCannotGrowMemory(requestedSize) {
    abort("OOM")
}
function _emscripten_resize_heap(requestedSize) {
    abortOnCannotGrowMemory(requestedSize)
}
var PATH = {
    splitPath: function(filename) {
        var splitPathRe = /^(\/?|)([\s\S]*?)((?:\.{1,2}|[^\/]+?|)(\.[^.\/]*|))(?:[\/]*)$/;
        return splitPathRe.exec(filename).slice(1)
    },
    normalizeArray: function(parts, allowAboveRoot) {
        var up = 0;
        for (var i = parts.length - 1; i >= 0; i--) {
            var last = parts[i];
            if (last === ".") {
                parts.splice(i, 1)
            } else if (last === "..") {
                parts.splice(i, 1);
                up++
            } else if (up) {
                parts.splice(i, 1);
                up--
            }
        }
        if (allowAboveRoot) {
            for (; up; up--) {
                parts.unshift("..")
            }
        }
        return parts
    },
    normalize: function(path) {
        var isAbsolute = path.charAt(0) === "/"
          , trailingSlash = path.substr(-1) === "/";
        path = PATH.normalizeArray(path.split("/").filter(function(p) {
            return !!p
        }), !isAbsolute).join("/");
        if (!path && !isAbsolute) {
            path = "."
        }
        if (path && trailingSlash) {
            path += "/"
        }
        return (isAbsolute ? "/" : "") + path
    },
    dirname: function(path) {
        var result = PATH.splitPath(path)
          , root = result[0]
          , dir = result[1];
        if (!root && !dir) {
            return "."
        }
        if (dir) {
            dir = dir.substr(0, dir.length - 1)
        }
        return root + dir
    },
    basename: function(path) {
        if (path === "/")
            return "/";
        var lastSlash = path.lastIndexOf("/");
        if (lastSlash === -1)
            return path;
        return path.substr(lastSlash + 1)
    },
    extname: function(path) {
        return PATH.splitPath(path)[3]
    },
    join: function() {
        var paths = Array.prototype.slice.call(arguments, 0);
        return PATH.normalize(paths.join("/"))
    },
    join2: function(l, r) {
        return PATH.normalize(l + "/" + r)
    }
};
var SYSCALLS = {
    buffers: [null, [], []],
    printChar: function(stream, curr) {
        var buffer = SYSCALLS.buffers[stream];
        if (curr === 0 || curr === 10) {
            (stream === 1 ? out : err)(UTF8ArrayToString(buffer, 0));
            buffer.length = 0
        } else {
            buffer.push(curr)
        }
    },
    varargs: 0,
    get: function(varargs) {
        SYSCALLS.varargs += 4;
        var ret = HEAP32[SYSCALLS.varargs - 4 >> 2];
        return ret
    },
    getStr: function() {
        var ret = UTF8ToString(SYSCALLS.get());
        return ret
    },
    get64: function() {
        var low = SYSCALLS.get()
          , high = SYSCALLS.get();
        return low
    },
    getZero: function() {
        SYSCALLS.get()
    }
};
function _fd_close(fd) {
    try {
        return 0
    } catch (e) {
        if (typeof FS === "undefined" || !(e instanceof FS.ErrnoError))
            abort(e);
        return e.errno
    }
}
function _fd_seek(fd, offset_low, offset_high, whence, newOffset) {
    try {
        return 0
    } catch (e) {
        if (typeof FS === "undefined" || !(e instanceof FS.ErrnoError))
            abort(e);
        return e.errno
    }
}
function _fd_write(fd, iov, iovcnt, pnum) {
    try {
        var num = 0;
        for (var i = 0; i < iovcnt; i++) {
            var ptr = HEAP32[iov + i * 8 >> 2];
            var len = HEAP32[iov + (i * 8 + 4) >> 2];
            for (var j = 0; j < len; j++) {
                SYSCALLS.printChar(fd, HEAPU8[ptr + j])
            }
            num += len
        }
        HEAP32[pnum >> 2] = num;
        return 0
    } catch (e) {
        if (typeof FS === "undefined" || !(e instanceof FS.ErrnoError))
            abort(e);
        return e.errno
    }
}
var asmLibraryArg = {
    "a": _abs,
    "c": _emscripten_memcpy_big,
    "d": _emscripten_resize_heap,
    "f": _fd_close,
    "b": _fd_seek,
    "e": _fd_write,
    "memory": wasmMemory,
    "table": wasmTable
};
var asm = createWasm();
Module["asm"] = asm;
var ___wasm_call_ctors = Module["___wasm_call_ctors"] = function() {
    return Module["asm"]["g"].apply(null, arguments)
}
;
var _opus_chunkdecoder_version = Module["_opus_chunkdecoder_version"] = function() {
    return Module["asm"]["h"].apply(null, arguments)
}
;
var _opus_chunkdecoder_enqueue = Module["_opus_chunkdecoder_enqueue"] = function() {
    return Module["asm"]["i"].apply(null, arguments)
}
;
var _opus_chunkdecoder_decode_float_stereo_deinterleaved = Module["_opus_chunkdecoder_decode_float_stereo_deinterleaved"] = function() {
    return Module["asm"]["j"].apply(null, arguments)
}
;
var _opus_chunkdecoder_create = Module["_opus_chunkdecoder_create"] = function() {
    return Module["asm"]["k"].apply(null, arguments)
}
;
var _malloc = Module["_malloc"] = function() {
    return Module["asm"]["l"].apply(null, arguments)
}
;
var _opus_chunkdecoder_free = Module["_opus_chunkdecoder_free"] = function() {
    return Module["asm"]["m"].apply(null, arguments)
}
;
var _free = Module["_free"] = function() {
    return Module["asm"]["n"].apply(null, arguments)
}
;
var _opus_get_version_string = Module["_opus_get_version_string"] = function() {
    return Module["asm"]["o"].apply(null, arguments)
}
;
var stackSave = Module["stackSave"] = function() {
    return Module["asm"]["p"].apply(null, arguments)
}
;
var stackAlloc = Module["stackAlloc"] = function() {
    return Module["asm"]["q"].apply(null, arguments)
}
;
var stackRestore = Module["stackRestore"] = function() {
    return Module["asm"]["r"].apply(null, arguments)
}
;
Module["asm"] = asm;
Module["cwrap"] = cwrap;
var calledRun;
function ExitStatus(status) {
    this.name = "ExitStatus";
    this.message = "Program terminated with exit(" + status + ")";
    this.status = status
}
dependenciesFulfilled = function runCaller() {
    if (!calledRun)
        run();
    if (!calledRun)
        dependenciesFulfilled = runCaller
}
;
function run(args) {
    args = args || arguments_;
    if (runDependencies > 0) {
        return
    }
    preRun();
    if (runDependencies > 0)
        return;
    function doRun() {
        if (calledRun)
            return;
        calledRun = true;
        if (ABORT)
            return;
        initRuntime();
        preMain();
        if (Module["onRuntimeInitialized"])
            Module["onRuntimeInitialized"]();
        postRun()
    }
    if (Module["setStatus"]) {
        Module["setStatus"]("Running...");
        setTimeout(function() {
            setTimeout(function() {
                Module["setStatus"]("")
            }, 1);
            doRun()
        }, 1)
    } else {
        doRun()
    }
}
Module["run"] = run;
if (Module["preInit"]) {
    if (typeof Module["preInit"] == "function")
        Module["preInit"] = [Module["preInit"]];
    while (Module["preInit"].length > 0) {
        Module["preInit"].pop()()
    }
}
noExitRuntime = true;
run();
Module["OpusStreamDecoder"] = OpusStreamDecoder;

function OpusStreamDecodedAudio(left, right, samplesDecoded) {
    this.left = left;
    this.right = right;
    this.samplesDecoded = samplesDecoded;
    this.sampleRate = 48e3
}
function OpusStreamDecoder(options) {
    if ("function" !== typeof options.onDecode)
        throw Error("onDecode callback is required.");
    Object.defineProperty(this, "onDecode", {
        value: options.onDecode
    })
}
OpusStreamDecoder.prototype.ready = new Promise(function(resolve, reject) {
    addOnPreMain(function() {
        var api = {
            malloc: cwrap("malloc", "number", ["number"]),
            free: cwrap("free", null, ["number"]),
            HEAPU8: HEAPU8,
            HEAPF32: HEAPF32,
            libopusVersion: cwrap("opus_get_version_string", "string", []),
            decoderVersion: cwrap("opus_chunkdecoder_version", "string", []),
            createDecoder: cwrap("opus_chunkdecoder_create", null, []),
            freeDecoder: cwrap("opus_chunkdecoder_free", null, ["number"]),
            enqueue: cwrap("opus_chunkdecoder_enqueue", null, ["number", "number", "number"]),
            decode: cwrap("opus_chunkdecoder_decode_float_stereo_deinterleaved", "number", ["number", "number", "number", "number"])
        };
        Object.freeze(api);
        Object.defineProperty(OpusStreamDecoder.prototype, "api", {
            value: api
        });
        resolve()
    })
}
);
OpusStreamDecoder.prototype.decode = function(uint8array) {
    if (!(uint8array instanceof Uint8Array))
        throw Error("Data to decode must be Uint8Array");
    if (!this._decoderPointer) {
        this._decoderPointer = this.api.createDecoder()
    }
    var srcPointer, decodedInterleavedPtr, decodedInterleavedArry, decodedLeftPtr, decodedLeftArry, decodedRightPtr, decodedRightArry;
    try {
        var decodedPcmSize = 120 * 48 * 2;
        [decodedInterleavedPtr,decodedInterleavedArry] = this.createOutputArray(decodedPcmSize);
        [decodedLeftPtr,decodedLeftArry] = this.createOutputArray(decodedPcmSize / 2);
        [decodedRightPtr,decodedRightArry] = this.createOutputArray(decodedPcmSize / 2);
        var sendMax = 16 * 1024, sendStart = 0, sendSize;
        var srcLen = uint8array.byteLength;
        srcPointer = this.api.malloc(uint8array.BYTES_PER_ELEMENT * sendMax);
        while (sendStart < srcLen) {
            sendSize = Math.min(sendMax, srcLen - sendStart);
            this.api.HEAPU8.set(uint8array.subarray(sendStart, sendStart + sendSize), srcPointer);
            sendStart += sendSize;
            if (!this.api.enqueue(this._decoderPointer, srcPointer, sendSize))
                throw Error("Could not enqueue bytes for decoding.  You may also have invalid Ogg Opus file.");
            var samplesDecoded, totalSamplesDecoded = 0;
            while (samplesDecoded = this.api.decode(this._decoderPointer, decodedInterleavedPtr, decodedPcmSize, decodedLeftPtr, decodedRightPtr)) {
                totalSamplesDecoded += samplesDecoded;
                this.onDecode(new OpusStreamDecodedAudio(decodedLeftArry.slice(0, samplesDecoded),decodedRightArry.slice(0, samplesDecoded),samplesDecoded))
            }
        }
    } catch (e) {
        throw e
    } finally {
        this.api.free(srcPointer);
        this.api.free(decodedInterleavedPtr);
        this.api.free(decodedLeftPtr);
        this.api.free(decodedRightPtr)
    }
}
;
OpusStreamDecoder.prototype.free = function() {
    if (this._decoderPointer) {
        this.api.freeDecoder(this._decoderPointer)
    }
}
;
Object.defineProperty(OpusStreamDecoder.prototype, "createOutputArray", {
    value: function(length) {
        var pointer = this.api.malloc(Float32Array.BYTES_PER_ELEMENT * length);
        var array = new Float32Array(this.api.HEAPF32.buffer,pointer,length);
        return [pointer, array]
    }
});




/*
// NODE
if ("undefined" !== typeof global && exports) {
    module.exports.OpusStreamDecoder = OpusStreamDecoder
}
*/

// [opus-stream-decoder.es6.js] - end
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

const SAMPLE_RATE = 48000;   // IMPORTANT! This OPUS decoder ALWAYS returns the decoded audio samples at 48000 Hz. This cannot be changed. So we need to work at this frequency.

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// 
/*

    =======================================
    decoded_audio_playback_buffer_class
    =======================================
    This is the buffer where the bytes decoded by the worker are stored (it contains decoded data)
  
  
    Dependencies:
    =============
    NOTE: dependencies listed with d are not direct dependencies of this file but indirect dependencies because of one of its descendants
      AudioStreamPlayer
  

    Props:
    ========
    NOTE: props listed with s flag are static  
    s   maxFlushSize
    s   maxGrows
    s   firstFlushLength
    s   growFactor

    
    Methods:
    ========
    NOTE: methods listed with s flag are static
    NOTE: methods starting with _ are private methods and should not be used by the caller
    NOTE: methods starting with $ are async methods that should be used with await/yield/promises
    s   flushLength(flushCount)
        constructor({ onFlush })
        reset()
        add({ left, right })
        flush()
        
    
*/
class decoded_audio_playback_buffer_class {
  // ----------------------------------------------------------------------------
  // use a 128K buffer
  static maxFlushSize = 1024 * 128;

  // exponentially grow over these many flushes
  // too small causes skips. 25 skips at 72kbps download, 64-kbit file
  static maxGrows = 50;

  // samples for for first flush. grow from here. 20ms @ SAMPLE_RATE Hz
  static firstFlushLength = 0.02 * SAMPLE_RATE;

  // exponential grow coefficient from firstFlushLength samples to maxFlushSize bytes
  // Floating point is 4 bytes per sample
  static growFactor = Math.pow(
    decoded_audio_playback_buffer_class.maxFlushSize / 4 / decoded_audio_playback_buffer_class.firstFlushLength,
    1 / (decoded_audio_playback_buffer_class.maxGrows - 1)
  );

  static flushLength = (flushCount) => {
    const flushes    = Math.min(flushCount, decoded_audio_playback_buffer_class.maxGrows - 1);
    const multiplier = Math.pow(decoded_audio_playback_buffer_class.growFactor, flushes);
    const length     = Math.round(decoded_audio_playback_buffer_class.firstFlushLength * multiplier);
    
    return length;
  }

  // left/right channels of buffers we're filling
  _bufferL; //= new Float32Array(decoded_audio_playback_buffer_class.maxFlushSize);
  _bufferR; //= new Float32Array(decoded_audio_playback_buffer_class.maxFlushSize);

  _bufferPos;      // last filled position in buffer
  _onFlush;        // user-provided function
  _flushCount;     // number of times we've already flushed

  // ----------------------------------------------------------------------------
  constructor({ onFlush }) {
    if (typeof onFlush !== 'function')  throw Error('onFlush must be a function');
    this._onFlush = onFlush;
    this.reset();
  }
  // ----------------------------------------------------------------------------
  reset() {
    this._bufferPos = 0;
    this._flushCount = 0;
    this._bufferL = new Float32Array(decoded_audio_playback_buffer_class.maxFlushSize);
    this._bufferR = new Float32Array(decoded_audio_playback_buffer_class.maxFlushSize);    
  }
  // ----------------------------------------------------------------------------
  add({ left, right }) {
    const srcLen = left.length;
    let bufferLen,
        srcStart = 0,
        bufferPos = this._bufferPos;

    while (srcStart < srcLen) {
      bufferLen = decoded_audio_playback_buffer_class.flushLength(this._flushCount);
      const len = Math.min(bufferLen - bufferPos, srcLen - srcStart);
      const end = srcStart + len;
      this._bufferL.set(left.slice(srcStart, end), bufferPos);
      this._bufferR.set(right.slice(srcStart, end), bufferPos);
      srcStart  += len;
      bufferPos += len;
      this._bufferPos = bufferPos;
      if (bufferPos === bufferLen) {
        this.flush(bufferPos);
        bufferPos = 0;
      }
    }
  }
  // ----------------------------------------------------------------------------
  flush() {
    const bufferPos = this._bufferPos;
    this._onFlush({
      left:this._bufferL.slice(0, bufferPos),
      right:this._bufferR.slice(0, bufferPos)
    });
    this._flushCount++;
    this._bufferPos = 0;
  }
  // ----------------------------------------------------------------------------
}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~




// ======
// WORKER
// ======

let session_id, session_decoder_id, request_id, flush_timeout_id;

self.onmessage                      = $on__worker__received_message;
var opus_stream_decoders            = [];
var opus_stream_decoder             = null;
var decoded_audio_playback_buffer   = new decoded_audio_playback_buffer_class({ onFlush:on__decoded_audio_playback_buffer__flush });
var garbage_collection__interval_id = null;

// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
function check_if_session_has_changed(session_new_id, session_decoder_new_id, request_new_id) {
  // detect new session and reset decoder
  request_id = request_new_id;
  
  if (opus_stream_decoder &&
      session_id && session_id===session_new_id &&
      session_id && session_decoder_id===session_decoder_new_id)  return;

  // We need to create a new Opus Decoder.
  // And this means that the old one will NOT be used anymore.
  // We are assuming that at this stage the old decoder will NOT receive any more data associated to it.
  // From this moment on, all data is supposed to be associated to the new decoder.
  console.log('[RESETING THE DECODER!!!!!] - session_id(old):['+session_id+']  session_id(new):['+session_new_id+']  session_decoder_id(old):['+session_decoder_id+']  session_decoder_id(new):['+session_decoder_new_id+']');
  
  session_id = session_new_id;
  session_decoder_id = session_decoder_new_id;  
  
  decoded_audio_playback_buffer.flush();
  decoded_audio_playback_buffer.reset();
  
  // Free up the decoder's memory in WebAssembly (also resets decoder for reuse)
  // See: https://www.npmjs.com/package/opus-stream-decoder
  // The following alternative ways to do this did not work for me:
  // opus_stream_decoder.free();
  // opus_stream_decoder.ready.then(_ => opus_stream_decoder.free());
  var garbage_collection__fn = function() {
    //console.log('[GARBAGE_COLLECTION] - ndecoders:'+opus_stream_decoders.length);
    // If there are more than 1 opus_stream_decoders, it is time now to remove the oldest one (this interval time for some seconds allows some time for the decoder to decode chunks that it might be still processing)
    if (opus_stream_decoders.length>1)   { opus_stream_decoders.shift().free(); console.log('  [OLDEST_DECODER_REMOVED] - ndecoders:'+opus_stream_decoders.length); }
    if (opus_stream_decoders.length<=1)  { clearInterval(garbage_collection__interval_id); garbage_collection__interval_id = null; }
  };
  var on__opus_stream_decoder__ready = function() {
    opus_stream_decoder = new OpusStreamDecoder({ onDecode:on__opus_stream_decoder__decode });
    opus_stream_decoders.push(opus_stream_decoder);
    console.log('[NEW_OPUSSTREAMDECODER_CREATED] - ndecoders:'+opus_stream_decoders.length);
    garbage_collection__fn();
    if (garbage_collection__interval_id==null) {
      garbage_collection__interval_id = setInterval(garbage_collection__fn, 1000);  // IMPORTANT!!! This needs to be smaller than Math.min(PLAY_SEGMENTS_DURATION_MS, PLAY_SEGMENT1_DURATION_MS)
    }
  };
  if (opus_stream_decoder!=null)  opus_stream_decoder.ready.then(_ => on__opus_stream_decoder__ready());
  else                            on__opus_stream_decoder__ready();
  
};
// ----------------------------------------------------------------------------
async function $on__worker__received_message(evt) {
  let { chunk_in_encoded__bytes, chunk_in_encoded__id, session_id, session_decoder_id, request_id } = evt.data;
  //console.log('  [received_chunk_by_decoder] - chunk_encoded__id:['+chunk_in_encoded__id+']  chunk_encoded__nbytes:['+chunk_in_encoded__bytes.byteLength+']  session_id:['+session_id+']  session_decoder_id:['+session_decoder_id+']');
  check_if_session_has_changed(session_id, session_decoder_id, request_id);
  await opus_stream_decoder.ready;
  opus_stream_decoder.decode(new Uint8Array(chunk_in_encoded__bytes));  // This will call on__opus_stream_decoder__decode after it is done
};
// ----------------------------------------------------------------------------
function on__opus_stream_decoder__decode({ left, right, samplesDecoded, sampleRate }) {
  // Receives decoded Float32Array PCM audio in left/right arrays.
  // sampleRate is always 48000 and both channels would always contain data if
  // samplesDecoded > 0.  Mono Opus files would decoded identically into both
  // left/right channels and multichannel Opus files would be downmixed to 2 channels.
  //  
  // Decoder recovers when it receives new files, and samplesDecoded is negative.
  // For cause, see https://github.com/AnthumChris/opus-stream-decoder/issues/7
  if (samplesDecoded < 0)  return;
  decoded_audio_playback_buffer.add({ left, right});    // This will call on__decoded_audio_playback_buffer__flush one or multiple times (the way decoded_audio_playback_buffer flushes its buffer is tricky, it does not use a fixed size but it grows it over time)
  decoded_audio_playback_buffer__schedule_last_flush();    // Schedule a last flush (unless a new flush is invoked before the scheduled time for the last flush is reached)
};
// ----------------------------------------------------------------------------
function on__decoded_audio_playback_buffer__flush({ left, right }) {
  const chunk_out_decoded__data = {
    channelData: [left, right],
    length: left.length,
    numberOfChannels: 2,
    sampleRate: SAMPLE_RATE
  };
  self.postMessage(
    { chunk_out_decoded__data, session_id, session_decoder_id, request_id },
    [
      chunk_out_decoded__data.channelData[0].buffer,
      chunk_out_decoded__data.channelData[1].buffer
    ]
  );
};
// ----------------------------------------------------------------------------
// No End of file is signaled from decoder. This ensures last bytes always flushed
function decoded_audio_playback_buffer__schedule_last_flush() {
  //console.log('[decoded_audio_playback_buffer__schedule_last_flush]');
  clearTimeout(flush_timeout_id);
  flush_timeout_id = setTimeout(_ => {
    decoded_audio_playback_buffer.flush();
  }, 100);
};
// ----------------------------------------------------------------------------
