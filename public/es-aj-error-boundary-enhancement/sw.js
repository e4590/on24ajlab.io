const EPHEMERAL_STATIC_BASE_URL = 'https://e4590.gitlab.io/on24aj.gitlab.io/public/';
let assetList = [];
let isEphemeral = false;
let ephemeralLocation = '';

self.addEventListener('install', function (event) {
  event.waitUntil(self.skipWaiting());
});
self.addEventListener('activate', function (event) {
  event.waitUntil(self.clients.claim());
});

const publicFolderBaseUrl = new URL(self.location.href).pathname.replace('sw.js', '');

self.addEventListener('fetch', function (e) {
  if (e.request.method !== 'GET') return;
  const requestUrl = new URL(e.request.url);
  // Remove extraneous path segments added by elite studio. For example, all paths starting with
  // https://pmeliteqa.on24.com/view/presenterCustomFolder/dev/pmelite/p4workspace/public/
  // will start with / instead. This makes the path irrelevant, so it works in QA or localhost.
  const pathname = requestUrl.pathname.replace(publicFolderBaseUrl, '/');

  // When the initial index.html file is loaded, check if they have an ephemeral location
  // specified in the query string param. If so, load index.html from there and set isEphemeral = true;
  if (pathname === '/' && e.request.headers.get('accept').includes('text/html')) {
    ephemeralLocation = requestUrl.searchParams.get('eph');
    if (ephemeralLocation) {
      // since this is a request for the main index.html file it should set isEphemeral for the rest of the requests
      isEphemeral = true;
      e.respondWith(
        (async () => {
          const res = await fetch(
            `${EPHEMERAL_STATIC_BASE_URL}${ephemeralLocation}/asset-list.log`
          );
          assetList = (await res.text()).split('\n');
          return await fetch(`${EPHEMERAL_STATIC_BASE_URL}${ephemeralLocation}/index.html`);
        })()
      );
    } else {
      isEphemeral = false;
    }
    // if isEphemeral is true and the request is a file which exists in the specified ephemeral location,
    // then return the file from there
  } else if (isEphemeral && assetList.includes(pathname)) {
    // TODO: Gitlab Pages isn't working with gzipped files, so requesting the regular version for now.
    // I think this may be causing an issue with the map files as well
    e.respondWith(
      fetch(`${EPHEMERAL_STATIC_BASE_URL}${ephemeralLocation}${pathname.replace('.gz', '')}`)
    );
  }
  // if neither check above is true, just let the browser do its thing without interference
});
