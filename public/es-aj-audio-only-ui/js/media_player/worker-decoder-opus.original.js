import { OpusStreamDecoder } from './opus-stream-decoder.es6.js';  // See: https://web.dev/module-workers/
//import { decoded_audio_playback_buffer_class } from 'decoded-audio-playback-buffer.mjs';

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

const SAMPLE_RATE = 48000;   // IMPORTANT! This OPUS decoder ALWAYS returns the decoded audio samples at 48000 Hz. This cannot be changed. So we need to work at this frequency.

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// 
/*

    =======================================
    decoded_audio_playback_buffer_class
    =======================================
    This is the buffer where the bytes decoded by the worker are stored (it contains decoded data)
  
  
    Dependencies:
    =============
    NOTE: dependencies listed with d are not direct dependencies of this file but indirect dependencies because of one of its descendants
      AudioStreamPlayer
  

    Props:
    ========
    NOTE: props listed with s flag are static  
    s   maxFlushSize
    s   maxGrows
    s   firstFlushLength
    s   growFactor

    
    Methods:
    ========
    NOTE: methods listed with s flag are static
    NOTE: methods starting with _ are private methods and should not be used by the caller
    NOTE: methods starting with $ are async methods that should be used with await/yield/promises
    s   flushLength(flushCount)
        constructor({ onFlush })
        reset()
        add({ left, right })
        flush()
        
    
*/
class decoded_audio_playback_buffer_class {
  // ----------------------------------------------------------------------------
  // use a 128K buffer
  static maxFlushSize = 1024 * 128;

  // exponentially grow over these many flushes
  // too small causes skips. 25 skips at 72kbps download, 64-kbit file
  static maxGrows = 50;

  // samples for for first flush. grow from here. 20ms @ SAMPLE_RATE Hz
  static firstFlushLength = 0.02 * SAMPLE_RATE;

  // exponential grow coefficient from firstFlushLength samples to maxFlushSize bytes
  // Floating point is 4 bytes per sample
  static growFactor = Math.pow(
    decoded_audio_playback_buffer_class.maxFlushSize / 4 / decoded_audio_playback_buffer_class.firstFlushLength,
    1 / (decoded_audio_playback_buffer_class.maxGrows - 1)
  );

  static flushLength = (flushCount) => {
    const flushes    = Math.min(flushCount, decoded_audio_playback_buffer_class.maxGrows - 1);
    const multiplier = Math.pow(decoded_audio_playback_buffer_class.growFactor, flushes);
    const length     = Math.round(decoded_audio_playback_buffer_class.firstFlushLength * multiplier);
    
    return length;
  }

  // left/right channels of buffers we're filling
  _bufferL; //= new Float32Array(decoded_audio_playback_buffer_class.maxFlushSize);
  _bufferR; //= new Float32Array(decoded_audio_playback_buffer_class.maxFlushSize);

  _bufferPos;      // last filled position in buffer
  _onFlush;        // user-provided function
  _flushCount;     // number of times we've already flushed

  // ----------------------------------------------------------------------------
  constructor({ onFlush }) {
    if (typeof onFlush !== 'function')  throw Error('onFlush must be a function');
    this._onFlush = onFlush;
    this.reset();
  }
  // ----------------------------------------------------------------------------
  reset() {
    this._bufferPos = 0;
    this._flushCount = 0;
    this._bufferL = new Float32Array(decoded_audio_playback_buffer_class.maxFlushSize);
    this._bufferR = new Float32Array(decoded_audio_playback_buffer_class.maxFlushSize);    
  }
  // ----------------------------------------------------------------------------
  add({ left, right }) {
    const srcLen = left.length;
    let bufferLen,
        srcStart = 0,
        bufferPos = this._bufferPos;

    while (srcStart < srcLen) {
      bufferLen = decoded_audio_playback_buffer_class.flushLength(this._flushCount);
      const len = Math.min(bufferLen - bufferPos, srcLen - srcStart);
      const end = srcStart + len;
      this._bufferL.set(left.slice(srcStart, end), bufferPos);
      this._bufferR.set(right.slice(srcStart, end), bufferPos);
      srcStart  += len;
      bufferPos += len;
      this._bufferPos = bufferPos;
      if (bufferPos === bufferLen) {
        this.flush(bufferPos);
        bufferPos = 0;
      }
    }
  }
  // ----------------------------------------------------------------------------
  flush() {
    const bufferPos = this._bufferPos;
    this._onFlush({
      left:this._bufferL.slice(0, bufferPos),
      right:this._bufferR.slice(0, bufferPos)
    });
    this._flushCount++;
    this._bufferPos = 0;
  }
  // ----------------------------------------------------------------------------
}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~




// ======
// WORKER
// ======

let session_id, session_decoder_id, request_id, flush_timeout_id;

self.onmessage                      = $on__worker__received_message;
var opus_stream_decoders            = [];
var opus_stream_decoder             = null;
var decoded_audio_playback_buffer   = new decoded_audio_playback_buffer_class({ onFlush:on__decoded_audio_playback_buffer__flush });
var garbage_collection__interval_id = null;

// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
function check_if_session_has_changed(session_new_id, session_decoder_new_id, request_new_id) {
  // detect new session and reset decoder
  request_id = request_new_id;
  
  if (opus_stream_decoder &&
      session_id && session_id===session_new_id &&
      session_id && session_decoder_id===session_decoder_new_id)  return;

  session_id = session_new_id;
  session_decoder_id = session_decoder_new_id;  
  
  console.log('[RESETING THE DECODER!!!!!]');
  decoded_audio_playback_buffer.flush();
  decoded_audio_playback_buffer.reset();
  
  // Free up the decoder's memory in WebAssembly (also resets decoder for reuse)
  // See: https://www.npmjs.com/package/opus-stream-decoder
  // The following alternative ways to do this did not work for me:
  // opus_stream_decoder.free();
  // opus_stream_decoder.ready.then(_ => opus_stream_decoder.free());
  var on__opus_stream_decoder__ready = function() {
    opus_stream_decoder = new OpusStreamDecoder({ onDecode:on__opus_stream_decoder__decode });
    opus_stream_decoders.push(opus_stream_decoder);
    console.log('[NEW_OPUSSTREAMDECODER_CREATED] - ndecoders:'+opus_stream_decoders.length);
    if (garbage_collection__interval_id==null) {
      garbage_collection__interval_id = setInterval(function() {
        console.log('[GARBAGE_COLLECTION] - ndecoders:'+opus_stream_decoders.length);
        // If there are more than 1 opus_stream_decoders, it is time now to remove the oldest one (this interval time for some seconds allows some time for the decoder to decode chunks that it might be still processing)
        if (opus_stream_decoders.length>1)   { opus_stream_decoders.shift().free(); console.log('  [OLDEST_DECODER_REMOVED] - ndecoders:'+opus_stream_decoders.length); }
        if (opus_stream_decoders.length<=1)  { clearInterval(garbage_collection__interval_id); garbage_collection__interval_id = null; }
      }, 10000);
    }
  };
  if (opus_stream_decoder!=null)  opus_stream_decoder.ready.then(_ => on__opus_stream_decoder__ready());
  else                            on__opus_stream_decoder__ready();
  
};
// ----------------------------------------------------------------------------
async function $on__worker__received_message(evt) {
  let { chunk_in_encoded__bytes, chunk_in_encoded__id, session_id, session_decoder_id, request_id } = evt.data;
  //console.log('  [received_chunk_by_decoder] - chunk_encoded__id:['+chunk_in_encoded__id+']  chunk_encoded__nbytes:['+chunk_in_encoded__bytes.byteLength+']  session_id:['+session_id+']  session_decoder_id:['+session_decoder_id+']');
  check_if_session_has_changed(session_id, session_decoder_id, request_id);
  await opus_stream_decoder.ready;
  opus_stream_decoder.decode(new Uint8Array(chunk_in_encoded__bytes));  // This will call on__opus_stream_decoder__decode after it is done
};
// ----------------------------------------------------------------------------
function on__opus_stream_decoder__decode({ left, right, samplesDecoded, sampleRate }) {
  // Receives decoded Float32Array PCM audio in left/right arrays.
  // sampleRate is always 48000 and both channels would always contain data if
  // samplesDecoded > 0.  Mono Opus files would decoded identically into both
  // left/right channels and multichannel Opus files would be downmixed to 2 channels.
  //  
  // Decoder recovers when it receives new files, and samplesDecoded is negative.
  // For cause, see https://github.com/AnthumChris/opus-stream-decoder/issues/7
  if (samplesDecoded < 0)  return;
  decoded_audio_playback_buffer.add({ left, right});    // This will call on__decoded_audio_playback_buffer__flush one or multiple times (the way decoded_audio_playback_buffer flushes its buffer is tricky, it does not use a fixed size but it grows it over time)
  decoded_audio_playback_buffer__schedule_last_flush();    // Schedule a last flush (unless a new flush is invoked before the scheduled time for the last flush is reached)
};
// ----------------------------------------------------------------------------
function on__decoded_audio_playback_buffer__flush({ left, right }) {
  const chunk_out_decoded__data = {
    channelData: [left, right],
    length: left.length,
    numberOfChannels: 2,
    sampleRate: SAMPLE_RATE
  };
  self.postMessage(
    { chunk_out_decoded__data, session_id, session_decoder_id, request_id },
    [
      chunk_out_decoded__data.channelData[0].buffer,
      chunk_out_decoded__data.channelData[1].buffer
    ]
  );
};
// ----------------------------------------------------------------------------
// No End of file is signaled from decoder. This ensures last bytes always flushed
function decoded_audio_playback_buffer__schedule_last_flush() {
  //console.log('[decoded_audio_playback_buffer__schedule_last_flush]');
  clearTimeout(flush_timeout_id);
  flush_timeout_id = setTimeout(_ => {
    decoded_audio_playback_buffer.flush();
  }, 100);
};
// ----------------------------------------------------------------------------
