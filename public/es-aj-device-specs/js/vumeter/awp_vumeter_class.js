// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
/*
  =======================================
  awp_vumeter_class
  =======================================
  By Alejandro Castera Garcia - alejandro.castera@on24.com
  Scope: AudioWorkletGlobalScope
  . This is the processor worklet associated to the 'awn_vumeter_class'.

  
  Props:
  ========
  NOTE: props listed with s flag are static  
      convert_update_interval_ms_to_nframes

  
  Methods:
  ========
  NOTE: methods listed with s flag are static
  NOTE: methods starting with _ are private methods and should not be used by the caller
  NOTE: methods starting with $ are async methods that should be used with await/yield/promises
      constructor({ numberOfInputs, channelCountMode, channelCount, numberOfOutputs, processorOptions:{ audio_worklet_node_id, update_interval_ms, rms_peak_stay_ms } })
      process(inputs, outputs, parameters)

      
*/
const MINIMUM_LINEAR_VALUE = 0.00001;
const NSAMPLES_PER_FRAME = 1;
  // See: https://stackoverflow.com/a/19589884/3621841
  // For PCM the sample rate and the frame rate are the same since a frame consists of 1 sample from each channel:
  //   Sample rate = number of samples / second
  //   Frame = 1 sample from each channel (PCM)
  //   Frame Size = Sample size * Channels
  //   Frame Rate = frames / second    
const REF_DBFS = (1/Math.sqrt(2));
  // See: https://en.wikipedia.org/wiki/DBFS
  // See: https://dsp.stackexchange.com/a/70898
  // A full-sin wave    should have an RMS equal to 0 dBFs (See: https://en.wikipedia.org/wiki/DBFS)
  // A full-square wave should have an RMS equal to 3 dBFs (See: https://en.wikipedia.org/wiki/DBFS)
  // The RMS of a full-sin wave of period T is equal to Vpk*1/√2 (See: https://www.electronics-tutorials.ws/accircuits/rms-voltage.html)
  //   So if T = 4, RMS = 1*1/√2 = 1/√2 = 0.7071067811865475
  // The RMS of a full-square wave of period T is equal to Vpk (See: https://www.rfcafe.com/references/electrical/square-wave-voltage-conversion.htm)
  //   So if T = 4, RMS = 1
  // So the ref (REF_DBFS) to use is 1/√2 = 0.7071067811865475 ~= 0.7071
  //   . Full-sin    wave:   20 * Math.log10( 0.7071 / 0.7071 ) = 0 dBFs
  //   . Full-square wave:   20 * Math.log10(      1 / 0.7071 ) = 3 dBFs
  // The ref is the RMS value of a full-sin wave, which is Math.sqrt((0+1**2+0+1**2+0)/4) = Math.sqrt(1/2) = 0.7071067811865476   See: https://www.electronics-tutorials.ws/accircuits/rms-voltage.html
  // This is done so that 20 * Math.log10( rms_of_full_sin / ref ) == 0
  // Since rms_of_full_sin == 0.7071, then  0 dBFs = 20 * Math.log10( rms_of_full_sin / ref ) = 20 * Math.log10( 0.7071 / ref )  ==>  ref = 0.7071
  // So if whe receive an RMS value, we need to divide it by this reference


registerProcessor('awp_vumeter_class', class extends AudioWorkletProcessor {
  // ----------------------------------------------------------------------------
  constructor(options) {
    super();
    options.processorOptions.update_interval_ms = parseInt(options.processorOptions.update_interval_ms); if (isNaN(options.processorOptions.update_interval_ms)==true || options.processorOptions.update_interval_ms<=0)  options.processorOptions.update_interval_ms = 1000;
    options.processorOptions.rms_peak_stay_ms = parseInt(options.processorOptions.rms_peak_stay_ms); if (isNaN(options.processorOptions.rms_peak_stay_ms)==true || options.processorOptions.rms_peak_stay_ms<=0)  options.processorOptions.rms_peak_stay_ms = 1000*3;
    console.log('[awp_vumeter_class] - ' + JSON.stringify(options));
    this._rms;
    this._rms_peak;
    this._rms_peak_last_updated_timestamp;
    this._audio_worklet_node_id  = options.processorOptions.audio_worklet_node_id;
    this._update_interval_ms     = options.processorOptions.update_interval_ms;
    this._rms_peak_stay_ms       = options.processorOptions.rms_peak_stay_ms;
      this._rms_peak_stay_ms = Math.floor(this._rms_peak_stay_ms/this._update_interval_ms);
      if (this._rms_peak_stay_ms<1)  this._rms_peak_stay_ms = 1;
      this._rms_peak_stay_ms = this._rms_peak_stay_ms * this._update_interval_ms;
    this._rms_window_ms          = options.processorOptions.rms_window_ms;          // It should be around 300ms
    this._next_update_frame      = 0 + this.convert_update_interval_ms_to_nframes;  // We will update when we process the frame number 0 + nframes (in _update_interval_ms duration)
    this._ongoing = [];
    // Receive message from the associated AudioWorkletNode (the 'vumeter_node' in our case)
    this.port.onmessage = event => {
      if (event.data.audio_worklet_node_id!=this._audio_worklet_node_id)  return;
      if (event.data.update_interval_ms)  this._update_interval_ms = event.data.update_interval_ms;
    }
  }
  // ----------------------------------------------------------------------------
  get convert_update_interval_ms_to_nframes() {
    // See: https://stackoverflow.com/a/19589884/3621841
    var nsamples = this._update_interval_ms/1000 * sampleRate;  // sampleRate is one of the variables in AudioWorkletGlobalScope (which is the scope shared across AudioWorkletProcessors. Other variables include: currentFrame / currentTime / sampleRate (see: https://developer.mozilla.org/en-US/docs/Web/API/AudioWorkletGlobalScope)
    var nframes  = nsamples / NSAMPLES_PER_FRAME;
    return nframes;
  }
  // ----------------------------------------------------------------------------
  process(inputs, outputs, parameters) {
    // inputs[iinput][ichannel][isample]
    // This method gets called for each block of 128 sample-frames and takes input and output arrays and calculated values of custom AudioParams (if they are defined) as parameters.
    // You can use inputs and audio parameter values to fill the outputs array, which by default holds silence.
    // Each sample value is in range of [-1, +1]  (with decimals, since we get a Float32Array)
    // So values are normalized. BUT!!!! You can actually get values out of the [-1, +1] range.
    // For instance if you have 5 oscillators connected to 1 input of an audio node, all those 5 inputs will be mixed down so levels will add-up, giving you values in the [-5, +5] range.
    // But any values out of the [-1, +1] range are considered as clipped

    // See: vumeter_node_class.js where we specify numberOfInputs==I, channelCount==C, which means that all connections will be mixed to just I inputs, each one of them with C channels (so channels from each connections are also mixed to C channels)
    // NOTE: If 0 inputs are connected then 0 channels will be passed in.
    var temp, ninputs, nchannels, ongoing, inputI_channels, inputI_channelC_samples, inputI_channelC_nsamples,
        arr1, arr2, i, c, s,
        ssum, rms, current_timestamp, keep_processing, reset;
    
    if (false) {
      // Detect if all samples in input.channel0 are zero
      try {
        temp = true;
        for(let s of inputs[0][0]) { 
          if (s!=0)  { temp = false; break; }
        }
        //temp = JSON.stringify( inputs[0][0] );
      } catch(e) { temp = null; }            
      temp = '[process] - ' +
          'currentTime:['+currentTime+']' +
        '  currentFrame:['+currentFrame+']' +
        '  sampleRate:['+sampleRate+']' +
        '  ninputs:['+inputs.length+']' +
        '  input0.nchannels:['+((inputs[0]!=null && inputs[0].length>=0)?inputs[0].length:0)+']' +
        '  input0.channel0.nsamples:['+((inputs[0]!=null && inputs[0].length>=0 && inputs[0][0]!=null && inputs[0][0].length>=0)?inputs[0][0].length:0)+']' +
        '  input0.channel0.sample0:['+((inputs[0]!=null && inputs[0][0]!=null && inputs[0][0][0]!=null)?inputs[0][0][0]:0)+']' +
        '  input0.channel0.all_samples_are_zero:['+temp+']' +
        '';
      //console.log(temp);
      this.port.postMessage({ audio_worklet_node_id:this._audio_worklet_node_id, log:temp });
    }
    
    // Initialize this._rms and this._rms_peak if they have NOT been initialized yet
    reset = false;
    try { ninputs = inputs.length; } catch(e) { inputs = []; ninputs = 0; }
    if (reset==false && (this._rms==null || this._rms.length!=ninputs))  reset = true;
    for (i=0; i<ninputs; i++) {
      try { nchannels = inputs[i].length; } catch(e) { inputs[i][c] = []; nchannels = 0; }
      if (reset==false && (this._rms[i]==null || this._rms[i].length!=nchannels))  reset = true;
    }
    if (reset==true || this._rms==null || this._rms_peak==null || this._rms_peak_last_updated_timestamp==null) {
      this._rms = [];
      this._rms_peak = [];
      this._rms_peak_last_updated_timestamp = [];
      this._ongoing = [];
        for (i=0; i<ninputs; i++) {
          this._rms[i] = [];
          this._rms_peak[i] = [];
          this._rms_peak_last_updated_timestamp[i] = [];
          this._ongoing[i] = [];
          for (c=0; c<inputs[i].length; c++) {
            this._rms[i][c] = 0.0;
            this._rms_peak[i][c] = 0.0;
            this._rms_peak_last_updated_timestamp[i][c] = null;
            this._ongoing[i][c] = {   // This object (one per input per channel) is used to store details of ongoing computations that are needed to calculate the RMS value.
              samples: [],            // This is a circular array that will contain samples of a window of   this._rms_window_ms   milliseconds.
              ssum: 0,                // This is the cummulative square-sum.
              nsample_circular_cursor: 0 - 1,
              nsamples_window: Math.floor(this._rms_window_ms/1000 * sampleRate)
            };
          }
        }
    }

    
    // Start processing inputs and store samples in our circular array
    // We will also compute the cummulative square-sum (ssum)
    inputI_channelC_nsamples = null;
    for (i=0; i<ninputs; i++) {
      inputI_channels = inputs[i];
      nchannels = inputI_channels.length;
      for (c=0; c<nchannels; c++) {
        inputI_channelC_samples = inputI_channels[c];
          if (inputI_channelC_nsamples==null || inputI_channelC_nsamples<inputI_channelC_samples.length)  inputI_channelC_nsamples = inputI_channelC_samples.length;
        ongoing = this._ongoing[i][c];
        for (s=0; s<inputI_channelC_samples.length; ++s) {
          ongoing.nsample_circular_cursor = (ongoing.nsample_circular_cursor + 1) % ongoing.nsamples_window;
            if ((temp = ongoing.samples[ongoing.nsample_circular_cursor])!=null)  ongoing.ssum -= (temp**2);  // If the cursor points to an existing old sample, let's remove its square value from ssum and replace the sample with the new one
          temp = inputI_channelC_samples[s];
          ongoing.samples[ongoing.nsample_circular_cursor] = temp;
          ongoing.ssum += (temp**2);   // Squared-sum. Note this will also get rid of negative values, so the new range will be [0, 1*1]
        }
      }
    }
    
    
    // Update and sync the RMS and RMS_PEAK properties with the main thread
    if (inputI_channelC_nsamples!=null) {
      this._next_update_frame -= (inputI_channelC_nsamples / NSAMPLES_PER_FRAME);
      if (this._next_update_frame<0) {
        // Time to send the updated values to the main thread!!!
        this._next_update_frame += this.convert_update_interval_ms_to_nframes;  // We will update when we process the frame number current_frame + nframes (in _update_interval_ms duration)
        
        // Calculate the RMS levels
        for (i=0; i<ninputs; i++) {
          inputI_channels = inputs[i];
          nchannels = inputI_channels.length;
          for (c=0; c<nchannels; c++) {
            ongoing = this._ongoing[i][c];
            rms = ongoing.samples.length>0 ? Math.sqrt(ongoing.ssum / ongoing.samples.length) : 0;
            if ((this._rms_peak[i][c]==null ||
                 this._rms_peak[i][c]<rms) ||
                (this._rms_peak[i][c]>=rms && (this._rms_peak_last_updated_timestamp[i][c]==null || (Date.now()-this._rms_peak_last_updated_timestamp[i][c])>=this._rms_peak_stay_ms))) {
              this._rms_peak[i][c] = rms;
              this._rms_peak_last_updated_timestamp[i][c] = Date.now();
            }
            this._rms[i][c] = rms; // Fast attack
          }
        }
        
        //console.log('  [process] - rms:'+JSON.stringify(this._rms)+'  rms_peak:'+JSON.stringify(this._rms_peak)+'');
        // Send message to the associated AudioWorkletNode (the 'vumeter_node' in our case)
        this.port.postMessage({ audio_worklet_node_id:this._audio_worklet_node_id, rms:this._rms, rms_peak:this._rms_peak });
      }
    }
     
     
    // Keep on processing if the volume is above a threshold, so that disconnecting inputs does not immediately cause the meter to stop computing its smoothed value
    keep_processing = false;
    if (false) {
      // Let's deactivate this functionality for now. Needs more testing.
      for (i=0; keep_processing==false && i<ninputs; i++) {
        for (c=0; keep_processing==false && c<nchannels; c++) {
          if (this._rms[i][c]>=MINIMUM_LINEAR_VALUE)  { keep_processing = true; break; }
        }
      }
    } else {
      keep_processing = true;
    }
    
    return keep_processing;
  }
  // ----------------------------------------------------------------------------
});
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
